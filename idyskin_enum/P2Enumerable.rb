module P2Enumerable
	def p2collect
		r = Array.new
		#iterate over all elements e
		p2each do |e|
			res = (yield e)
			r << res
		end
		return r
	end

	def p2collect_concat
		r = Array.new
		p2each do |e|
			a = []
			#a << (yield e)
			r.concat(yield e)
		end
		return r
	end

	def p2all?
		p2each do |e|
			if (yield e) == false
				return false
			end
		end
		return true
	end

	def p2any?
		p2each do |e|
			if (yield e) == true
				return true
			end
		end
		return false
	end

	def p2count
		cnt = 0
		p2each do |e|
			if (yield e) == true
				cnt += 1
			end
		end
		return cnt
	end

	def p2cycle(n = -1)
		while n != 0
			p2each do |e|
				yield e
			end
			n -= 1
		end
	end

	def p2detect(n = nil)
		p2each do |e|
			if (yield e) == true
				return e
			end
		end
		return n
	end

	def p2drop(n)
		res = Array.new
		p2each do |e|
			if n > 0
				n -= 1
			else
				res << e
			end
		end
		return res
	end

	def p2drop_while
		counter = 0
		p2each do |e|
			counter += 1
			if !yield (e)
				counter -= 1
				break
			end
		end
		self.p2drop(counter)
	end

	def p2each_cons(n)
		arr = []
		p2each do |e|
			if arr.length < n
				arr << e
			else
				yield arr
				arr = arr[1...arr.length]
				arr << e
			end
		end
		if arr.length == n
			yield arr
		end
	end


	def p2each_slice(n)
		interim = 0
		interim_arr = Array.new
		p2each do |e|
			if interim == n
				yield interim_arr
				interim = 1
				interim_arr = Array.new
				interim_arr << e
			else
				interim += 1
				interim_arr << e
			end
		end
		if !interim_arr.empty?
				yield interim_arr
			end
	end

	def p2each_with_index
		cnt = 0
		p2each do |e|
			(yield e, cnt)
			cnt += 1
		end
	end

	def p2entries
		arr = Array.new
		p2each do |e|
			arr << e
		end
		return arr
	end

	alias p2find p2detect

	def p2find_all
		arr = Array.new
		p2each do |e|
			r = yield e
			if r
				arr << e
			end
		end
		return arr
	end

	def p2find_index
		cnt = 0
		p2each do |e|
			if yield e
				return cnt
			end
			cnt += 1
		end
	end

	def p2first(n = 0)
		arr = Array.new
		p2each do |e|
			if n == 0
				if !arr.empty?
					return arr
				else
					return e
				end
			end
			arr << e
			n -= 1
		end
		return arr
	end

	def p2group_by
		hash = Hash.new
		p2each do |e|
			#r = yield e
			(hash[yield e] ||= []) << e
		end
		return hash
	end

	def p2inject( init )
    r = init
    p2each do | e |
      r = yield( r, e )
    end
    return r
  end

  def p2para0inject
    first = true
    r = nil
    self.p2each do | e |
      if first
        r = e
        first = false
      else
        r = yield( r, e )
      end
    end
    return r
  end

  def p2minmax
  	min = self.p2entries[0]
  	max = self.p2entries[0]
  	p2each do |e|
  		if (yield e, min) == -1
  			min = e
  		end
  		if (yield e, max) == 1
  			max = e
  		end
  	end
  	return [min, max]
  end

  def p2minmax_by
  	min = self.p2entries[0]
  	max = self.p2entries[0]
  	p2each do |e|
  		r = yield e
  		if r < (yield min)
  			min = e
  		end
  		if r > (yield max)
  			max = e
  		end
  	end
  	return [min, max]
  end

  def p2partition
  	true_arr = Array.new
  	false_arr = Array.new
  	p2each do |e|
  		if yield e
  			true_arr << e
  	else
  		false_arr << e
  	end
  end
  	return [true_arr, false_arr]
  end

  def p2reject
  	false_arr = Array.new
  	p2each do |e|
  		if !(yield e)
  			false_arr << e
  		end
  	end
  	return false_arr
  end

  alias p2take p2first

  def p2take_while
  	arr = Array.new
  	p2each do |e|
  		if !(yield e)
  			return arr
  		end
  		arr << e
  	end
  	return arr
  end

  alias p2to_a p2entries

  def p2to_h
  	hash = Hash.new
  	p2each do |e|
  		hash[e[0]] = e[1]
  	end
  	return hash
  end

end

class Array
	include P2Enumerable
	#alias p2each each
end

class Hash
	include P2Enumerable
end

class Array
	def p2each
		index = 0
		while index < self.size
			yield self[index]
			index += 1
		end
	end
end

class Hash
	def p2each
		index = 0
		arr = self.to_a
		while index < arr.size
			yield arr[index]
			index += 1
		end
	end
end