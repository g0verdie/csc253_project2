require_relative '../idyskin_enum/P2Enumerable.rb'

def test_p2collect
	r = [1,2,3].p2collect{|e| e+1}
	raise "#{__method__} error" if r != [2,3,4]
	p "#{__method__} passed"
end

def test_p2collect_concat
	r = [[1,2],[3,4]].p2collect_concat{|e| e + [100]}
	raise "#{__method__} error" if r != [1, 2, 100, 3, 4, 100] 
	p "#{__method__} passed"
end

def test_p2all?
	raise "#{__method__} error" if [1,2,3,4,5].p2all?{|e| e > 6} != false and [1,2,3,4,5].p2all?{|e| e % 3 == 0} != true
	p "#{__method__} passed"
end

def test_p2any?
	raise "#{__method__} error" if [1,2,3,4,5].p2any?{|e| e % 3} != true and [1,2,3,4,5].p2any?{|e| e % 6} != false
	p "#{__method__} passed"
end

def test_p2count
	raise "#{__method__} error" if [1,2,3,4,5].p2count{|e| e < 4} != 3 
	p "#{__method__} passed"
end

def test_p2cycle
	a = ["a","b","c","d"]
	b = Array.new
	a.p2cycle(2) { |x| b << x }
	raise "#{__method__}error" if b.length != 8
	
	p "#{__method__} passed"   
end

def test_p2detect
	a = [1,2,3,4,70].p2detect { |i| i % 5 == 0 and i % 7 == 0 }
	b = [1,2,3,4,5].p2detect { |i| i % 5 == 0 and i % 7 == 0 }
	raise "#{__method__}error" if a != 70
	raise "#{__method__}error" if b != nil
	p "#{__method__} passed"   
end

def test_p2drop
	a = [1,2,3,4,5,0]
	a = a.p2drop(3)
	raise "#{__method__}error" if a.length != 3
	p "#{__method__} passed"
	
end

def test_p2drop_while
	a = [1,2,3,4,5,0]
	a = a.p2drop_while { |i| i < 3}
	raise "#{__method__} error" if a != [3,4,5,0]
	p "#{__method__} passed"
end

def test_p2each_cons
	b = Array.new
	[1,2,3,4,5,6,7,8,9,10].p2each_cons(3) { |a| b<<a }
	raise "#{__method__} error" if b.length != 8
	p "#{__method__} passed"
end

def test_p2each_slice
	b = Array.new
	[1,2,3,4,5,6,7,8,9,10].p2each_slice(3) { |a| b<<a }
	raise "#{__method__} error" if b.length != 4
	p "#{__method__} passed"
end

def test_p2each_with_index
	hash = Hash.new
	%w(cat dog wombat).p2each_with_index { |item, index|
		hash[item] = index
	}
	raise "#{__method__} error" if (hash["cat"]!=0 || hash["dog"]!=1 || hash["wombat"]!=2)
	p "#{__method__} passed" 
end

def test_p2entries
	a = [1,2,3,4,5,6,7].p2entries
	b = { 'a'=>1, 'b'=>2, 'c'=>3 }.p2entries
	raise "#{__method__} error" if a.length != 7
	raise "#{__method__} error" if b.length != 3

	p "#{__method__} passed"

end

def test_p2find
	raise "#{__method__} error" if [1,2,3].p2find{|i| i < 3} != 1
	p "#{__method__} passed"
end

def test_p2find_all
	arr = [1,2,3].p2find_all{|i| i < 3}
	raise "#{__method__} error" if arr != [1,2]
	p "#{__method__} passed"
end

def test_p2find_index
	raise "#{__method__} error" if [1,2,3].p2find_index{|i| i < 3} != 0
	p "#{__method__} passed"
end

def test_p2first
	raise "#{__method__} error" if [1,2,3].first(2) != [1,2]
	p "#{__method__} passed"
end

def test_p2group_by
	raise "#{__method__} error" if [1,2,3,4,5,6].p2group_by { |i| i%3} != {1=>[1, 4], 2=>[2, 5], 0=>[3, 6]} 
	p "#{__method__} passed"
end

def test_p2inject
	raise "#{__method__} error" if [5,6,7,8,9,10].p2inject(1) { |product, n| product * n } != 151200
	p "#{__method__} passed"
end

def test_p2para0inject
	raise "#{__method__} error" if [5,6,7,8,9,10].p2para0inject {|sum, n| sum + n} != 45
	p "#{__method__} passed"
end

def test_p2minmax
	raise "#{__method__} error" if [1,2,3,4,5,6,7,8,9].p2minmax{|a,b| a <=> b} != [1,9]
	p "#{__method__} passed"
end

def test_p2minmax_by
	raise "#{__method__} error" if [1,2,3,4,5,6,7,8,9].p2minmax_by{|i| i} != [1,9]
	p "#{__method__} passed"
end

def test_p2partition
	raise "#{__method__} error" if [1,2,3,4,5,6,7,8,9].p2partition{|i| i % 3 == 0} != [[3,6,9], [1,2,4,5,7,8]]
	p "#{__method__} passed"
end

def test_p2reject
	raise "#{__method__} error" if [1,2,3,4,5,6,7,8,9].p2reject{|i| i % 3 == 0} != [1,2,4,5,7,8]
	p "#{__method__} passed"
end

def test_p2take_while
	raise "#{__method__} error" if [1,2,3,4,5,6,7,8,9].p2take_while{|i| i % 3 != 0} != [1,2]
	p "#{__method__} passed"
end

def test_p2to_h
	raise "#{__method__} error" if [[1,2],[3,4]].p2to_h != {1=>2, 3=>4}
	p "#{__method__} passed"
end

test_p2collect
test_p2collect_concat
test_p2all?
test_p2any?
test_p2count
test_p2cycle
test_p2detect
test_p2drop
test_p2drop_while
test_p2each_cons
test_p2each_slice
test_p2each_with_index
test_p2entries
test_p2find
test_p2find_all
test_p2find_index
test_p2first
test_p2group_by
test_p2inject
test_p2para0inject
test_p2minmax
test_p2minmax_by
test_p2partition
test_p2reject
test_p2take_while
test_p2to_h